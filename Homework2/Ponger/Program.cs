﻿using RabbitMQ.Wrapper;
using System;

namespace Ponger
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Ponger");
			Wrapper wrapper = new Wrapper("Exchange", "Ping", "ping", "Exchange", "Pong", "pong");
			wrapper.ListenQueue(() =>
			{
				wrapper.SendMessageToQueue("Pong");
			});
		}
	}
}
