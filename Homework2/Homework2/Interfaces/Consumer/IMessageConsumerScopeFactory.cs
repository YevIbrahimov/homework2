﻿using RabbitMQ.Models;

namespace RabbitMQ.Interfaces
{
    public interface IMessageConsumerScopeFactory
    {
        public IMessageConsumerScope Open(MessageScopeSettings messageScopeSettings);
        public IMessageConsumerScope Connect(MessageScopeSettings messageScopeSettings);
    }
}
