﻿using RabbitMQ.Client.Events;
using System;

namespace RabbitMQ.Interfaces
{
    public interface IMessageConsumer
    {
        public event EventHandler<BasicDeliverEventArgs> Received; 
        public void Connect();
        public void SetAcknowledge(ulong deliveryTag, bool processed);
    }
}
