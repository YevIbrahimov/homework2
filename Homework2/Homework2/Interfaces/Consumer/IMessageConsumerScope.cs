﻿namespace RabbitMQ.Interfaces
{
    public interface IMessageConsumerScope
    {
        public IMessageConsumer MessageConsumer{ get; }
    }
}
