﻿namespace RabbitMQ.Services
{
    public class ConnectionFactory
    {
        public static RabbitMQ.Client.ConnectionFactory SetConection() 
        {
            return new RabbitMQ.Client.ConnectionFactory()
            {
                HostName = "localhost",
                Port = 1488,
                UserName = "root",
                Password = "root"
            };
        }
    }  
}
