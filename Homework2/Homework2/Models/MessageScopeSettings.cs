﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RabbitMQ.Models
{
    public class MessageScopeSettings
    {
        public string Exchange { get; set; }
        public string Queue { get; set; }
        public string RoutingKey { get; set; }
        public string ExhangeType { get; set; }
    }
}
